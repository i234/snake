package snake.struct;

import java.io.InputStream;
import java.io.OutputStream;

public class StructUserList {
	OutputStream os;
	InputStream is;
	OutputStream ros;
	InputStream ris;
	String status, username;
	
	
	public StructUserList(OutputStream os, InputStream is, String status) {
		this.os = os;
		this.is = is;
		this.status = status;
	}
	
	/*
	public StructUserList(OutputStream os, InputStream is, OutputStream ros, InputStream ris, String status) {
		this.os = os;
		this.is = is;
		this.status = status;
	}*/
	
	
	public void setOutputStream(OutputStream os) {
		this.os = os;
	}
	
	public OutputStream getOutputStream() {
		return os;
	}
	
	
	public void setInputStream(InputStream is) {
		this.is = is;
	}
	
	public InputStream getInputStream() {
		return is;
	}
	
	
	public void setrivalOutputStream(OutputStream ros) {
		this.ros = ros;
	}
	
	public OutputStream getrivalOutputStream() {
		return ros;
	}
	
	
	public void setrivalInputStream(InputStream ris) {
		this.ris = ris;
	}
	
	public InputStream getrivalInputStream() {
		return ris;
	}
	
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	
}
