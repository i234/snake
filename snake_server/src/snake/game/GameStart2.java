package snake.game;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import org.json.JSONObject;

import new_snake_server.Json;
import snake.struct.StructUserList;

public class GameStart2 extends Thread {
	OutputStream os;
	InputStream is;
	OutputStream ros;
	InputStream ris;
	String str;
	JSONObject j;
	StructUserList player;
	
	public GameStart2(StructUserList player) {
		this.player = player;
		this.os = player.getOutputStream();
		this.is = player.getInputStream();
		this.ros = player.getrivalOutputStream();
		this.ris = player.getrivalInputStream();
	}
	
	
	@Override
	public void run() {    //將訊息轉送給對方玩家
		// TODO Auto-generated method stub
		DataInputStream br = new DataInputStream(is);
		DataOutputStream bw = new DataOutputStream(ros);
		
		while(true) {
			try {
				str = br.readUTF();
				System.out.println(str);
				//Document doc = DocumentHelper.parseText(str);
				//j = new JSONObject(str);
				//Json.ParsingJson(j);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("遊戲中:接收玩家JSON失敗");
				break;
			}
			
			try {
				bw.writeUTF(str);
				bw.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("遊戲中:轉發JSON失敗");
				break;
			}
		}
		
		
	}
	
	
}
