package snake.game;

import java.util.LinkedList;
import java.util.Random;

import snake.struct.Direction;
import snake.struct.Node;

public class SnakePlayer {
	LinkedList<Node> snake;
	Direction dir = Direction.RIGHT;
	Boolean addsnakebody = false;
	Node food, snakeHead;
	int interval = 15;
	
	
	public void init() {
		snake = createSnake();
		food = createFood();
	}
	
	
	private LinkedList<Node> createSnake() {
		LinkedList<Node> snake = new LinkedList<>();
		
		snake.addFirst(new Node(1,1));
		snake.addFirst(new Node(2,1));
		snake.addFirst(new Node(3,1));
		snake.addFirst(new Node(4,1));
		snake.addFirst(new Node(5,1));
		
		return snake;
	}
	
	
	public Node createFood() {
		Random ran = new Random();
		Node temp = new Node(ran.nextInt(interval),ran.nextInt(interval));
    	
    	for(int i = 0; i <= snake.size(); i++) {
    		if(temp == snake.get(i)) {
    			temp = new Node(ran.nextInt(interval),ran.nextInt(interval));
    			i = 0;
    		}
    	}
		return temp;
	}
	
	
	public void snakeMove() {
		
		snakeHead = snake.getFirst();
		
		switch(dir) {
			case UP:
				snake.addFirst(new Node(snakeHead.getX(), snakeHead.getY()-1));
			case RIGHT:
				snake.addFirst(new Node(snakeHead.getX()+1, snakeHead.getY()));
			case DOWN:
				snake.addFirst(new Node(snakeHead.getX(), snakeHead.getY()+1));
			case LEFT:
				snake.addFirst(new Node(snakeHead.getX()-1, snakeHead.getY()));
		}
		
		//若對方吃到食物，我方增加長度
		if(addsnakebody = false)
			snake.removeLast();
		else
			addsnakebody = false;
	}
	
	
	public void checkEvent() {
		snakeHead = this.snake.getFirst();

		//吃到蛇身
		for(int i = 1; i <= snake.size(); i++) {
    		if(snakeHead == snake.get(i)) {
    			
    		}
    	}
		
		//碰到邊界
		if(snakeHead.getX() < 0 || snakeHead.getX() > interval-1 || snakeHead.getY() < 0 || snakeHead.getY() > interval-1 ) {
			
		}
		
		//吃到食物
		if(snakeHead == food) {
			
		}
	}
	
	
	public LinkedList<Node> getSnake() {
		return snake;
	}
	
	
	public void setDirection(Direction dir) {
		this.dir = dir;
	}
	
}
