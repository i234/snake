package snake.game;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;

import new_snake_server.XmlGame;

public class GameStart {
	
	SnakePlayer p1 = new SnakePlayer();
	
	
	public static void main (String args[]){

		GameStart gs = new GameStart();
		
	}
	
	
	public GameStart() {
		
		p1.init();
		
		Thread t = new Thread(new GameProcess());
		t.start();
		
	}

	
	public class GameProcess implements Runnable{
		
		OutputStream outputstream;

		public GameProcess(){
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			p1.snakeMove();
			p1.checkEvent();
			
			
			//XmlGame.SendXml(XmlGame.BuildSnakeXml(p1.getSnake()), outputstream);
			//XmlGame.SendXml(XmlGame.BuildSnakeXml(p2.getSnake()), outputstream);
			
		}
	}
	
	//接收玩家回傳的事件
	public class ReceiveEvent implements Runnable{
		
		InputStream inputstram;
		OutputStream outputstream;
		BufferedReader br;
		String str;

		public ReceiveEvent(){
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			DataInputStream br = new DataInputStream(inputstram);
			try {
				str = br.readUTF();
				System.out.println(str);
				Document doc = DocumentHelper.parseText(str);
				XmlGame.ParsingXml(doc, p1);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	
}
