package snake.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.ResultSetMetaData;


public class DatabaseConnector {
	private static Connection con = null;
	private static Statement stat = null; 
	private static ResultSet rs = null;  
	private static PreparedStatement pst = null;
	
	static {
		try{
            //active mysql context
            Class.forName(DatabaseConfig.driver);
            
            con = DriverManager.getConnection(
                    DatabaseConfig.host,
                    DatabaseConfig.uname,
                    DatabaseConfig.upass);
            }catch(Exception e){
                System.out.println(e.toString());
            }
	}

	
	public static void register(String username,String account,String password){
	    try 
	    {
	    	String insertdbSQL = "INSERT INTO `member`(`username`, `account`, `password`) VALUES (?,?,?)"; 
	    	pst = con.prepareStatement(insertdbSQL);
	      
	    	pst.setString(1, username); 
	    	pst.setString(2, account); 
	    	pst.setString(3, password); 
	    	pst.executeUpdate(); 
	    	//JOptionPane.showMessageDialog(null, "註冊成功");
	    } 
	    catch(SQLException e) 
	    { 
	    	System.out.println("InsertDB Exception :" + e.toString()); 
	    } 
	    finally 
	    { 
	    	Close(); 
	    } 
	}
	
	
	public int login(String account,String password){
		try 
	    { 
	    	String selectdbSQL = "SELECT * FROM `member` WHERE `username` = " + account + " and `password` = " + account;
	    	Statement stmt = con.createStatement();
	    	ResultSet rs = stmt.executeQuery(selectdbSQL);
	    	java.sql.ResultSetMetaData rsmd = rs.getMetaData();
	    	int numColumns = rsmd.getColumnCount();
	    	
	    	//JOptionPane.showMessageDialog(null, "註冊成功");
	    	if(numColumns != 0) {
	    		return 1;
	    	}
	    } 
	    catch(SQLException e) 
	    { 
	    	System.out.println("InsertDB Exception :" + e.toString()); 
	    } 
	    finally 
	    { 
	    	Close(); 
	    }
		return 0; 
	} 
	
	
	private static void Close(){ 
		try{ 
			if(rs!=null){ 
				rs.close(); 
				rs = null; 
			} 
			if(stat!=null){ 
	    	  stat.close(); 
	    	  stat = null; 
			} 
			if(pst!=null){ 
	    	  pst.close(); 
	    	  pst = null; 
			} 
	    } 
	    catch(SQLException e){ 
	    	System.out.println("Close Exception :" + e.toString()); 
	    } 
	} 
	
	
}
