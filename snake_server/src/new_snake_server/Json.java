package new_snake_server;
import java.awt.Color;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.json.*;

import snake.struct.Node;
import snake.struct.StructUserList;

public class Json {
	
	//static Drawing drawrival;
	
	
	public static JSONObject BuildJson(String type){
		
		JSONObject json = new JSONObject();

		json.put("type", type);
		json.put("event", "");
		
        return json;
    }
	
	
	
	public static JSONObject BuildJson(LinkedList<Node> snake, Node food, String event){
		
		JSONObject json = new JSONObject();
		JSONArray jsnake = new JSONArray();
		String temp;
		
		//建立蛇身JSON
		for (int i = 0, size = snake.size(); i <= (size - 1); i++) {
			temp = snake.get(i).getX() + "," + snake.get(i).getY();
			jsnake.put(temp);
		}

		json.put("snake", jsnake);
		json.put("food", food.getX() + "," + food.getY());
		json.put("event", event);
        
        return json;
    }
	
	
	
	//送出XML
	public static void SendJson(JSONObject json, OutputStream outputstream) {
		  try {
			  String text = json.toString();
			  
			  DataOutputStream bw = new DataOutputStream(outputstream);
			  bw.writeUTF(text);
			  bw.flush();
			  System.out.println(text);
			  

		  } catch (IOException e) {
		   e.printStackTrace();
		  }
	}
	
	public static void ParsingJson(JSONObject json, StructUserList StructUser) {
		
		switch(json.getString("type")) {
			case "login":  //登入
				break;
				
			case "register":  //註冊
				break;
				
			case "pair":  //配對遊戲玩家
				pair(StructUser);
				break;
				
			case "gaming":  //遊戲中
				
				
				ParsingJsonForGame(json, StructUser);
				break;
				
		}
	}	
	
	
	public static void pair(StructUserList StructUser) {
		
		LinkedList<StructUserList> userlist, pairuserlist;
		StructUserList battleuser;
		
		pairuserlist = new LinkedList<>();
		userlist = Server.getUserList();
		
		
		
		for(int i = 0; i < userlist.size(); i++) {
			if(userlist.get(i).getStatus() == "pair") {  //如果有玩家處於配對狀態
				StructUser.setStatus("gaming");
				userlist.get(i).setStatus("gaming");
				
				StructUser.setrivalInputStream(userlist.get(i).getInputStream());
				StructUser.setrivalOutputStream(userlist.get(i).getOutputStream());
				
				userlist.get(i).setrivalInputStream(StructUser.getInputStream());
				userlist.get(i).setrivalOutputStream(StructUser.getOutputStream());
				
				SendJson(BuildJson("gaming"), StructUser.getOutputStream());
				SendJson(BuildJson("gaming"), userlist.get(i).getOutputStream());
			}
		}
		if(StructUser.getStatus() != "gaming") {
			StructUser.setStatus("pair");
		}
	
	}
	
	
	public static void ParsingJsonForGame(JSONObject json, StructUserList StructUser) {
		
		String text = json.toString();
		DataOutputStream bw = new DataOutputStream(StructUser.getrivalOutputStream());
		
		try {
			bw.writeUTF(text);
			bw.flush();
			System.out.print(text + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
