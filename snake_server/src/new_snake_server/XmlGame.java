package new_snake_server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import snake.game.SnakePlayer;
import snake.struct.Direction;
import snake.struct.Node;

public class XmlGame {
	static Element root;
	static String type;
	
	public static void ParsingXml(Document doc, SnakePlayer sp) {
		root = doc.getRootElement();
		type = root.element("type").getText();
		switch(type) {
			case "direction":
				changeDirection(doc, sp);
				
				
		}
	}
	
	
	public static void changeDirection(Document doc, SnakePlayer sp) {
		root.element("username").getText();
		Direction dir = null;
		
		sp.setDirection(dir);
;	}
	
	
	public static void Register(Document doc) {
		String username = root.element("username").getText();
		String account = root.element("account").getText();
		String password = root.element("password").getText();
		
		Db.register(username, account, password);
	}
	
	public static void Login(Document doc) {
		
	}
	

	public static Document BuildRegisterXml(String username, String account, String password){
		Document doc = DocumentHelper.createDocument();

        // 建立根元素(父元素)：
        Element root = doc.addElement("snake");
        //------------------------------------------------------
        // 內容(子元素)：
        Element root_type = root.addElement("type"); // 在<根>底下建立 <用戶>
        Element root_username = root.addElement("username");
        Element root_account = root.addElement("account");
        Element root_password = root.addElement("password");
        root_type.addText("register");
        root_username.addText(username);
        root_account.addText(account);
        root_password.addText(password);
        
        return doc;
    }
	
	
	public static Document BuildSnakeXml(LinkedList<Node> snake){
		Document doc = DocumentHelper.createDocument();
		Element temp;
        int x, y;
        
        Element root = doc.addElement("snake");
        Element root_type = root.addElement("type");
        root_type.addText("snake");
        
        for (int i = 0; i <= snake.size(); i++) {
        	x = snake.get(i).getX();
        	y = snake.get(i).getY();
        	
        	temp = root.addElement("body" + i);
        	temp.addText(Integer.toString(x) + "," + Integer.toString(y));
        }
        
        return doc;
    }
	
	
	public static void SendXml(Document doc, OutputStream outputstream) {
		  try {
			  String text = doc.asXML();
			  DataOutputStream bw = new DataOutputStream(outputstream);
			  bw.writeUTF(text);
			  bw.flush();
			  System.out.println(text);
			  

		  } catch (IOException e) {
		   e.printStackTrace();
		  }
	}
}
