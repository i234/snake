package new_snake_server;

import java.io.*;
import java.net.*;
import java.util.*;
import org.dom4j.*;
import org.dom4j.io.*;
import org.json.JSONObject;

import snake.game.GameStart2;
import snake.struct.StructUserList;

public class Server {
	static LinkedList<StructUserList> userlist = new LinkedList<>();

	public static void main (String args[]){
		Server server= new Server();
		server.ServerStart();
	}
	
	public void ServerStart() {
		ServerSocket serverSock = null;
		try {
			serverSock = new ServerSocket(8888);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(true){
					
			Socket cSocket = null;
			try {
				//等待連線的請求--串流
				cSocket = serverSock.accept();
				PrintStream writer = new PrintStream(cSocket.getOutputStream());
				userlist.add(new StructUserList(cSocket.getOutputStream(), cSocket.getInputStream(), "Start"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("等待Clinet連線階段 失敗");
				break;
			}    
	   		
			Thread t = new Thread(new Process(userlist.getLast())); //建立專門聆聽該連線的執行緒
			t.start();
			System.out.println(cSocket.getLocalSocketAddress()+ "有"+(t.activeCount()-1)+"個連接");
					//執行緒的在線次數
					//顯示連線人次
		}
	}
	
	
	public class Process implements Runnable{     //專門聆聽該連線的執行緒
		//暫存資料的Buffered
		//BufferedReader reader;
		StructUserList StructUser;
		BufferedReader br;
		String str;
		public Process(StructUserList StructUser){
			this.StructUser = StructUser;
			
			try{
				//取得Socket的輸入資料流
		   		//InputStreamReader isReader = new InputStreamReader(socket.getInputStream());
		   		//reader = new BufferedReader(isReader);
		   		
		   		/*
		   		if (userlist.size() >= 2) {
		   			userlist.get(0).setrivalInputStream(userlist.get(1).getInputStream());
		   			userlist.get(0).setrivalOutputStream(userlist.get(1).getOutputStream());
		   			userlist.get(1).setrivalInputStream(userlist.get(0).getInputStream());
		   			userlist.get(1).setrivalOutputStream(userlist.get(0).getOutputStream());
		   			GameStart2 gs1 = new GameStart2(userlist.get(0));
		   			GameStart2 gs2 = new GameStart2(userlist.get(1));
		   			gs1.start();
		   			gs2.start();
		   			JSONObject json = new JSONObject();
		   			json.put("event", "GameStart");
		   			Json.SendJson(json, userlist.get(0).getOutputStream());
		   			Json.SendJson(json, userlist.get(1).getOutputStream());
		   		}
		   		*/
		   		
		   		br = new BufferedReader(new InputStreamReader(StructUser.getInputStream()));
		   		
			}catch(Exception ex){
				System.out.println("連接失敗Process");
			} 
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			DataInputStream br = new DataInputStream(StructUser.getInputStream());
			JSONObject j;
			
			System.out.print("建立執行緒");
			while(true) {
				try {
					/*
					str = br.readUTF();
					System.out.println(str);
					Document doc = DocumentHelper.parseText(str);
					Xml.ParsingXml(doc);
					*/
					
					str = br.readUTF();
					
					j = new JSONObject(str);
					Json.ParsingJson(j, StructUser);
				
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("系統:接收使用者JSON失敗!");
					break;
				}
			}

			
		}
		
	}
	
	
	public static LinkedList<StructUserList> getUserList(){
		return userlist;
	}
}
