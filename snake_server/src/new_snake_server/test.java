package new_snake_server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class test {
		private static Connection con = null;
		private static Statement stat = null; 
		private static ResultSet rs = null;  
		private static PreparedStatement pst = null;
		
		public test() {
		    try { 
		    	Class.forName("com.mysql.jdbc.Driver"); 
		        con = DriverManager.getConnection( 
		        "jdbc:mysql://localhost/member?useUnicode=true&characterEncoding=utf-8", 
		        "root","1013--stdin"); 
		      } 
		      catch(ClassNotFoundException e) 
		      { 
		        System.out.println("DriverClassNotFound :"+e.toString()); 
		      }
		      catch(SQLException x) { 
		        System.out.println("Exception :"+x.toString()); 
		      }  
		}
		
		public static void register(String username,String account,String password){
		    try 
		    { 
		    	con = DriverManager.getConnection( 
		    	        "jdbc:mysql://localhost/snake?useUnicode=true&characterEncoding=utf-8", 
		    	        "root","1013--stdin"); 
		    	String insertdbSQL = "INSERT INTO `member`(`username`, `account`, `password`) VALUES (?,?,?)"; 
		    	pst = con.prepareStatement(insertdbSQL);
		      
		    	pst.setString(1, username); 
		    	pst.setString(2, account); 
		    	pst.setString(3, password); 
		    	pst.executeUpdate(); 
		    	JOptionPane.showMessageDialog(null, "註冊成功");
		    } 
		    catch(SQLException e) 
		    { 
		    	System.out.println("InsertDB Exception :" + e.toString()); 
		    } 
		    finally 
		    { 
		    	Close(); 
		    } 
		}
		
		public void SelectTable(){
			try{ 
				stat = con.createStatement(); 
				rs = stat.executeQuery(""); 
				System.out.println("ID\t\tName\t\tPASSWORD"); 
				while(rs.next()) 
				{ 
					System.out.println(rs.getInt("id")+"\t\t"+ 
		            rs.getString("name")+"\t\t"+rs.getString("passwd")); 
				} 
		    } 
		    catch(SQLException e){ 
		    	System.out.println("DropDB Exception :" + e.toString()); 
		    } 
		    finally{ 
		    	Close(); 
		    } 
		} 
		
		private static void Close(){ 
			try{ 
				if(rs!=null){ 
					rs.close(); 
					rs = null; 
				} 
				if(stat!=null){ 
		    	  stat.close(); 
		    	  stat = null; 
				} 
				if(pst!=null){ 
		    	  pst.close(); 
		    	  pst = null; 
				} 
		    } 
		    catch(SQLException e){ 
		    	System.out.println("Close Exception :" + e.toString()); 
		    } 
		} 
}
