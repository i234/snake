package new_snake_server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.dom4j.*;

public class Xml {
	static Element root;
	static String type;
	
	public static void ParsingXml(Document doc) {
		root = doc.getRootElement();
		type = root.element("type").getText();
		switch(type) {
			case "register":
				Register(doc);
				
			case "login":
				Login(doc);
				
		}
	}
	
	
	public static void Register(Document doc) {
		String username = root.element("username").getText();
		String account = root.element("account").getText();
		String password = root.element("password").getText();
		
		Db.register(username, account, password);
	}
	
	public static void Login(Document doc) {
		
	}
	

	public static Document BuildRegisterXml(String username, String account, String password){
		Document doc = DocumentHelper.createDocument();

        // 建立根元素(父元素)：
        Element root = doc.addElement("snake");
        //------------------------------------------------------
        // 內容(子元素)：
        Element root_type = root.addElement("type"); // 在<根>底下建立 <用戶>
        Element root_username = root.addElement("username");
        Element root_account = root.addElement("account");
        Element root_password = root.addElement("password");
        root_type.addText("register");
        root_username.addText(username);
        root_account.addText(account);
        root_password.addText(password);
        
        return doc;
    }
	
	
	public static void SendXml(Document doc, OutputStream outputstream) {
		  try {
			  String text = doc.asXML();
			  DataOutputStream bw = new DataOutputStream(outputstream);
			  bw.writeUTF(text);
			  bw.flush();
			  System.out.println(text);
			  

		  } catch (IOException e) {
		   e.printStackTrace();
		  }
	}
}
