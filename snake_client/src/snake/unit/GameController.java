package snake.unit;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import snake.game.Player;

public class GameController implements KeyListener {
	Player p1;

	public GameController(Player p1) {
		this.p1 = p1;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		switch(e.getKeyCode()) {
			case 37:
				if(p1.getDirection() != Direction.RIGHT)
					p1.setDirection(Direction.LEFT);
				break; 
			case 38:
				if(p1.getDirection() != Direction.DOWN)
					p1.setDirection(Direction.UP);
				break; 
			case 39:
				if(p1.getDirection() != Direction.LEFT)
					p1.setDirection(Direction.RIGHT);
				break; 
			case 40:
				if(p1.getDirection() != Direction.UP)
					p1.setDirection(Direction.DOWN);
				break; 
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
