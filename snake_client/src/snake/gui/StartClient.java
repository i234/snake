package snake.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

public class StartClient {
	static String name,ip="127.0.0.1";       
	static BufferedReader reader;           
	static PrintStream writer; 
	static Socket sock;
	
	static OutputStream outputstream;
	static InputStream inputstream;
	
	
	public static void main(String[] args) {
		Connection();
		
		//測試用
		//GuiSnakeGame frame = new GuiSnakeGame();
		//frame.setVisible(true);
		
		//正式用
		//Login login = new Login();
		//login.Start();
		
		//正式用
		Hall hell = new Hall();
		hell.setVisible(true);
	}
	
	
	private static void Connection(){
		try{
			//請求建立連線
			sock = new Socket(ip,8888);      
			//建立I/O資料流
			InputStreamReader streamReader = new InputStreamReader(sock.getInputStream());  
			//放入暫存區
			reader = new BufferedReader(streamReader);    
			//取得Socket的輸出資料流
			writer = new PrintStream(sock.getOutputStream());
			//連線成功
			System.out.println("網路建立-連線成功");    
			
			
			outputstream = sock.getOutputStream();
			inputstream = sock.getInputStream();
		 
		}catch(IOException ex ){
			System.out.println("建立連線失敗");
		}
	}
	
	
	public static OutputStream getOutputStream() {
		return outputstream;
	}
	
	
	public static InputStream getInputStream() {
		return inputstream;
	}
	
}
