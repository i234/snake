package snake.gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import snake.communication.Json;
import snake.game.Drawing;
import snake.game.Player;
import snake.game.thread.GameStart;
import snake.game.thread.ReceiveRival;
import snake.unit.Direction;
import snake.unit.GameController;

import java.awt.*;

public class GuiSnakeGame extends JFrame {
	
	//Graphics g;
	Drawing draw, draw2;

	private JPanel contentPane;
	private JPanel contentPane2;
	public static GameStart t;
	public static Boolean running = true;
	private static GuiGamePair wait;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		GuiSnakeGame frame = new GuiSnakeGame();
		frame.setVisible(true);
		
	}

	/**
	 * Create the frame.
	 */
	public GuiSnakeGame() {
		setTitle("貪食蛇");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 671, 1024);
		
		
		Player p1 = new Player();
		p1.init();
		
		//我方貪食蛇繪圖
		draw = new Drawing(0, 0, 30);
		draw.init(p1.getSnake(), p1.getFood());
		contentPane = draw.getCanvas();
		contentPane.setBounds(0, 0, 480, 480);
		getContentPane().add(contentPane);
		
		
		Player p2 = new Player();
		p2.init();
		
		//敵方貪食蛇繪圖
		draw2 = new Drawing(0, 0, 0);
		draw2.init(p2.getSnake(), p2.getFood());
		contentPane2 = draw2.getCanvas();
		contentPane2.setBounds(0, 500, 480, 480);
		getContentPane().add(contentPane2);
		
		Json.setDrawRival(draw2);
		
		t = new GameStart(p1, draw, running);
		Thread receiverival = new ReceiveRival(p1, StartClient.inputstream);
		
		
		//contentPane.setBounds(10, 10, 250, 250);
		
		//setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		
		//getContentPane().add(draw2.getCanvas());
		
		
		JLabel label = new JLabel("我方長度：");
		label.setBounds(491, 34, 80, 19);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("對方長度：");
		label_1.setBounds(491, 66, 80, 19);
		getContentPane().add(label_1);
		
		JButton button = new JButton("關閉");
		button.setBounds(491, 154, 99, 27);
		getContentPane().add(button);
		
		/*
		JPanel panel = new JPanel();
		panel.setBounds(34, 34, 264, 150);
		getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(45, 257, 253, 150);
		getContentPane().add(panel_1);
		*/
		
		addKeyListener(new GameController(p1));
		
		//t.start();
		receiverival.start();
		
		
		//contentPane.add(grap.getCanvas(),BorderLayout.CENTER);
		
		/*
		Container contentPane = this.getContentPane();
		contentPane.add(g.getCanvas());*/
		
		/*
		g=canvas.getGraphics();
		g.setClip(110,110, 300, 300);
		g.setColor(Color.BLACK);
		g.drawLine(0,0,50,50);
		draw = new Drawing(canvas.getGraphics());*/
		//g = canvas.getGraphics();
		//g.setClip(110,110, 300, 300); //設定g畫布的位置為表單的正中間。
		//g.setColor(Color.BLACK);
		
		wait = new GuiGamePair();
		wait.setVisible(true);
		
	}
	
	public static void GameStart() {
		wait.setVisible(false);
		t.start();
	}
}
