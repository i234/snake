package snake.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import snake.communication.Xml;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Register extends JFrame {

	private JPanel contentPane;
	private JTextField text_account;
	private JTextField text_username;
	private JTextField text_password;
	private JTextField text_checkpassword;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public Register() {
		setLocationRelativeTo(this);
		setTitle("\u8CAA\u98DF\u86C7-\u8A3B\u518A");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 338);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("\u91CD\u65B0\u8F38\u5165");
		button.setBounds(236, 236, 87, 23);
		contentPane.add(button);
		
		JLabel label = new JLabel("\u8A3B\u518A");
		label.setFont(new Font("�s�ө���", Font.PLAIN, 32));
		label.setBounds(175, 32, 117, 38);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("\u5E33\u865F");
		label_1.setBounds(133, 94, 46, 15);
		contentPane.add(label_1);
		
		text_account = new JTextField();
		text_account.setColumns(10);
		text_account.setBounds(189, 91, 96, 21);
		contentPane.add(text_account);
		
		JLabel label_2 = new JLabel("\u5BC6\u78BC");
		label_2.setBounds(133, 156, 46, 15);
		contentPane.add(label_2);
		
		text_username = new JTextField();
		text_username.setColumns(10);
		text_username.setBounds(189, 122, 96, 21);
		contentPane.add(text_username);
		
		JButton button_1 = new JButton("\u9001\u51FA");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username, account, password, checkpassword;
				username = text_username.getText();
				account = text_account.getText();
				password = text_password.getText();
				checkpassword = text_checkpassword.getText();
				if(password.equals(checkpassword)){
					//Db.register(username, account, password);
					Xml.SendXml(Xml.BuildRegisterXml(username, account, password), StartClient.outputstream);
				}
			}
		});
		button_1.setBounds(104, 236, 87, 23);
		contentPane.add(button_1);
		
		text_password = new JTextField();
		text_password.setBounds(189, 153, 96, 21);
		contentPane.add(text_password);
		text_password.setColumns(10);
		
		text_checkpassword = new JTextField();
		text_checkpassword.setBounds(189, 184, 96, 21);
		contentPane.add(text_checkpassword);
		text_checkpassword.setColumns(10);
		
		JLabel label_3 = new JLabel("\u78BA\u8A8D\u5BC6\u78BC");
		label_3.setBounds(121, 187, 58, 15);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("\u66B1\u7A31");
		label_4.setBounds(133, 125, 46, 15);
		contentPane.add(label_4);
	}
}
