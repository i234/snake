package snake.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import snake.communication.Json;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Hall extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hall frame = new Hall();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Hall() {
		setTitle("\u5927\u5EF3");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 409);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("開始配對");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {  //開始配對
				
				Json.SendJson(Json.BuildJson("pair"), StartClient.outputstream);
				
				GuiSnakeGame frame = new GuiSnakeGame();
				frame.setVisible(true);
				
			}
		});
		btnNewButton.setBounds(318, 25, 101, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("個人資料");
		btnNewButton_1.setBounds(318, 76, 101, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("離開遊戲");
		btnNewButton_2.setBounds(318, 128, 101, 23);
		contentPane.add(btnNewButton_2);
		
		JList list = new JList();
		list.setBounds(30, 25, 253, 312);
		contentPane.add(list);
	}
}
