package snake.communication;
import java.awt.Color;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.json.*;

import snake.game.Drawing;
import snake.game.Player;
import snake.gui.GuiSnakeGame;
import snake.unit.Node;

public class Json {
	
	static Drawing drawrival;
	static Boolean gamestart = false;
	
	public static JSONObject BuildJson(String type){
		
		JSONObject json = new JSONObject();

		json.put("type", type);
       
        return json;
    }
	
	public static JSONObject BuildsnakeJson(LinkedList<Node> snake, Node food, String event){
		
		JSONObject json = new JSONObject();
		JSONArray jsnake = new JSONArray();
		String temp;
		
		//建立蛇身JSON
		for (int i = 0, size = snake.size(); i <= (size - 1); i++) {
			temp = snake.get(i).getX() + "," + snake.get(i).getY();
			jsnake.put(i,temp);
		}
		json.put("type", "gaming");
		json.put("body", jsnake);
		json.put("food", food.getX() + "," + food.getY());
		json.put("event", event);
        
        return json;
    }
	
	
	
	//送出JSON
	public static void SendJson(JSONObject json, OutputStream outputstream) {
		  try {
			  String text = json.toString();
			  
			  DataOutputStream bw = new DataOutputStream(outputstream);
			  bw.writeUTF(text);
			  bw.flush();
			  //System.out.println(text);
			  

		  } catch (IOException e) {
		   e.printStackTrace();
		   GuiSnakeGame.running = false;
		  }
	}
	
	
	
	public static void ParsingJson(JSONObject json, Player p1) {
		
		LinkedList<Node> snake = new LinkedList<>();
		Node food;
		String[] bodyxy, foodxy;
		String temp;
		
		
		if(json.getString("type").equals("gaming") && gamestart == false) {  //遊戲進行中

			gamestart = true;
			GuiSnakeGame.GameStart();
			
		}else {
			//讀取對手蛇身資料
			for (int i = 0, size = json.getJSONArray("body").length(); i <= (size - 1); i++) {
				temp = json.getJSONArray("body").get(i).toString();
				bodyxy = temp.split(",");
				snake.add(new Node(Integer.valueOf(bodyxy[0]),Integer.valueOf(bodyxy[1])));
			}
			
			//讀取食物座標
			temp = json.getString("food");
			foodxy = temp.split(",");
			food = new Node(Integer.valueOf(foodxy[0]),Integer.valueOf(foodxy[1]));
			
			System.out.println("對手的："+json.toString());
			
			//繪製對手畫面
			drawrival.drawGaming(snake, food);
		}
		
		
		//System.out.println(json.getString("event"));
		//判斷事件
		if(json.getString("event") != null) {
			switch(json.getString("event")) {
			/*
			case "GameStart":
				GuiSnakeGame.GameStart();
				break;
			*/
			case "addsnakebody":
				p1.setAddSnakeBody(true);
				break;
				
			case "gameover":  //對方出局，我方勝利
				GuiSnakeGame.running = false;
				GuiSnakeGame.t.setRunning(false);
				break;
			}
		}

	}
	
	
	
	public static void setDrawRival(Drawing draw) {
		drawrival = draw;
	}
	
	
	
}
