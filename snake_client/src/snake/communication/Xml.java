package snake.communication;

import java.io.*;
import java.util.*;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.Transformer;  

import org.dom4j.*;
import org.dom4j.io.*;

import java.nio.charset.Charset;

import javax.swing.JOptionPane;

public class Xml {
	Element root;
	String type;
	
	
	//建立XML
	public static Document BuildRegisterXml(String username, String account, String password){
		Document doc = DocumentHelper.createDocument();

        // 建立根元素(父元素)：
        Element root = doc.addElement("snake");
        //------------------------------------------------------
        // 內容(子元素)：
        Element root_type = root.addElement("type"); // 在<根>底下建立 <用戶>
        Element root_username = root.addElement("username");
        Element root_account = root.addElement("account");
        Element root_password = root.addElement("password");
        root_type.addText("register");
        root_username.addText(username);
        root_account.addText(account);
        root_password.addText(password);
        
        return doc;
    }
	
	
	//送出XML
	public static void SendXml(Document doc, OutputStream outputstream) {
		  try {
			  String text = doc.asXML();
			  DataOutputStream bw = new DataOutputStream(outputstream);
			  bw.writeUTF(text);
			  bw.flush();
			  System.out.println(text);
			  

		  } catch (IOException e) {
		   e.printStackTrace();
		  }
	}
	
	
	//解析XML
	public void ParsingXml(Document doc) {
		root = doc.getRootElement();
		type = root.element("type").getText();
		switch(type) {
			case "message":
				
			case "login":
				
		}
	}
	
	
	public void Message(Document doc) {
		String message = root.element("message").getText();
		JOptionPane.showMessageDialog(null, message);
	}
	
	
}