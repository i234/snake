package snake.game;

import java.util.LinkedList;
import java.util.Random;

import snake.unit.Direction;
import snake.unit.Node;

public class Player {
	LinkedList<Node> snake;
	Direction  dir = Direction.RIGHT;
	Boolean addsnakebody = false;
	Node food, snakeHead, temp;
	int interval = 15;
	Random ran = new Random();
	String event;
	
	
	public void init() {
		snake = createSnake();
		food = createFood();
	}
	
	
	private LinkedList<Node> createSnake() {
		LinkedList<Node> snake = new LinkedList<>();
		
		snake.add(new Node(5,1));
		snake.add(new Node(4,1));
		snake.add(new Node(3,1));
		snake.add(new Node(2,1));
		snake.add(new Node(1,1));
		
		snakeHead = snake.getFirst();
		return snake;
	}
	
	
	public Node createFood() {
		temp = new Node(ran.nextInt(interval),ran.nextInt(interval));
    	
    	for(int i = 0, size = snake.size(); i <= (size - 1); i++) {
    		if(temp.getX() == snake.get(i).getX() && temp.getY() == snake.get(i).getY()) {
    			temp = new Node(ran.nextInt(interval),ran.nextInt(interval));
    			i = 0;
    		}
    	}
    	food = temp;
		return temp;
	}
	
	
	public void snakeMove() {
		switch(dir) {
			case UP:
				snake.addFirst(new Node(snakeHead.getX(), snakeHead.getY()-1));
				break; 
			case RIGHT:
				snake.addFirst(new Node(snakeHead.getX()+1, snakeHead.getY()));
				break; 
			case DOWN:
				snake.addFirst(new Node(snakeHead.getX(), snakeHead.getY()+1));
				break; 
			case LEFT:
				snake.addFirst(new Node(snakeHead.getX()-1, snakeHead.getY()));
				break; 
		}
		snakeHead = snake.getFirst();
		
		//若對方吃到食物，我方增加長度
		if(addsnakebody == false) 
			snake.removeLast();
		else
			addsnakebody = false;
	}
	
	
	public boolean checkEvent() {
		System.out.print(food.getX());
		System.out.print(food.getY());
		System.out.print("\n");
		System.out.print(snakeHead.getX());
		System.out.print(snakeHead.getY());
		System.out.print("\n");
		
		event = "";
		
		//吃到蛇身
		for(int i = 1; i <= snake.size()-1; i++) {
    		if(snakeHead.getX() == snake.get(i).getX() && snakeHead.getY() == snake.get(i).getY()) {
    			System.out.print("001!");
    			event = "gameover";
    			return false;
    		}
    	}
		
		//碰到邊界
		if(snakeHead.getX() < 0 || snakeHead.getX() > interval-1 || snakeHead.getY() < 0 || snakeHead.getY() > interval-1 ) {
			System.out.print("002!");
			event = "gameover";
			return false;
		}
		
		//吃到食物
		if(snakeHead.getX() == food.getX() && snakeHead.getY() == food.getY()) {
			System.out.print("003!");
			event = "addsnakebody";
			//addsnakebody= true;
			food = createFood();
		}
		return true;
		
	}
	
	
	public LinkedList<Node> getSnake() {
		return snake;
	}
	
	
	public Node getFood() {
		return food;
	}
	
	public void setSnake(LinkedList<Node> snake) {
		this.snake = snake;
	}
	
	
	public void setFood(Node food) {
		this.food = food;
	}
	
	
	public String getEvent() {
		return event;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}
	
	
	public void setDirection(Direction direction) {
		this.dir = direction;
	}
	
	public Direction getDirection() {
		return dir;
	}
	
	
	public  boolean gameOver() {
		System.out.print("gameover!");
		return false;
	}
	
	
	public void setAddSnakeBody(Boolean b) {
		this.addsnakebody = b;
	}
}
