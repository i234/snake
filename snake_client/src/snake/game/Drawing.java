package snake.game;

import java.awt.*;
import java.util.Random;
import java.util.LinkedList;

import javax.swing.JPanel;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import snake.unit.Node;

public class Drawing {
	
	LinkedList<Node> snake;
	private JPanel canvas;
	private Graphics graphics;
	private int foodx, foody, snakex, snakey, startx = 20, starty = 40;
	Node food;
	int interval;
	
	
	public Drawing (int x, int y, int interval) {
		this.interval = interval;
		this.startx = x;
		this.starty = y;
	}
	
	
	public void init(LinkedList<Node> snake2, Node food) {
		this.snake = snake2;
		this.food = food;
        canvas = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
            	graphics = g;
            	
            	drawCheckerBoard(15,15,interval);
            	
            	drawFood(interval);
            	
            	frawsnake(snake, interval);
            	//g.setClip(110,110, 300, 300);
        		//g.setColor(Color.BLACK);
        		//g.drawLine(0,0,50,50);
                //drawGridBackground(graphics);
                //drawSnake(graphics, grid.getSnake());
                //drawFood(graphics, grid.getFood());
            }
        };
		
	}
	
	
	public void drawGaming(LinkedList<Node> snake, Node food) {
		//canvas.paintComponents(graphics);
		//drawCheckerBoard(15,15,30);
		//drawFood(15, 15, 30);
    	//frawsnake(snake, 30);
		System.out.println("對手的食物："+food.getX());
		this.snake = snake;
		this.food = food;
    	canvas.repaint();
	}
	
	
	//棋盤
	public void drawCheckerBoard(int lattice_x, int lattice_y, int interval) {
		
		graphics.setColor(Color.gray);
		//畫出棋盤底部
		graphics.fillRect(0 + startx, 0 + starty, lattice_x * interval + 20, lattice_y * interval + 20);
		
		graphics.setColor(Color.white);
		//劃出橫向格線
		for (int i = 0; i <= lattice_x; i++) {
			graphics.drawLine(10 + startx, i * interval + 10 + starty, lattice_x * interval + 10 + startx, i * interval + 10 + starty);
		}
		//劃出縱向格線
		for (int i = 0; i <= lattice_y; i++) {
			graphics.drawLine(i * interval + 10 + startx, 10 + starty, i * interval + 10 + startx, lattice_y * interval + 10 + starty);
		}
		
	}
	
	
	//食物
	public void drawFood(int interval) {
		
		foodx = food.getX();
		foody = food.getY();
		
		graphics.setColor(Color.red);
		graphics.fillOval(foodx * interval + 10 + startx, foody * interval + 10 + starty, interval, interval);
	}
	
	
	// 蛇身
	public void frawsnake(LinkedList<Node> snake, int interval) {
		/*
		Document doc = DocumentHelper.createDocument();

		// 測試用XML
        Element root = doc.addElement("snake");
        //------------------------------------------------------
        Element root_type = root.addElement("type");
        Element body1 = root.addElement("body1");
        Element body2 = root.addElement("body2");
        Element body3 = root.addElement("body3");
        root_type.addText("snake_body");
        body1.addText("5,6");
        body2.addText("5,7");
        body3.addText("5,8");
		
		snake = doc;*/
		
		graphics.setColor(Color.yellow);
		for (int i = 0, size = snake.size(); i <= (size - 1); i++) {
			if (i > 0)
				graphics.setColor(Color.green);
			
			this.snakex = snake.get(i).getX();
			this.snakey = snake.get(i).getY();
			
			graphics.fillRect(this.snakex * interval + 10 + startx, this.snakey * interval + 10 + starty, interval, interval);
		}
		
		/*
		graphics.setColor(Color.yellow);
		root = snake.getRootElement();
		for (int i = 1, size = root.nodeCount(); i <= (size - 1); i++) {
			if (i > 1)
				graphics.setColor(Color.green);
			Node node = root.node(i);
			String body = node.getText();
			String[] bodyxy = body.split(",");
			this.snakex = Integer.valueOf(bodyxy[0]);
			this.snakey = Integer.valueOf(bodyxy[1]);
			
			graphics.fillRect(this.snakex * interval + 10, this.snakey * interval + 10, interval, interval);
		}*/
		
	}
	
	
	public JPanel getCanvas() {
		//canvas.setPreferredSize(500,500);
		return canvas;
	}
	
	
    public void draw() {
        canvas.repaint();
    }
}
