package snake.game.thread;

import java.io.*;

import org.dom4j.*;
import org.json.JSONObject;

import snake.communication.Json;
import snake.game.Player;

public class ReceiveRival extends Thread{
	InputStream inputstram;
	OutputStream outputstream;
	BufferedReader br;
	String str;
	JSONObject j;
	Player p1;

	public ReceiveRival(Player p1, InputStream inputstram){
		this.p1 = p1;
		this.inputstram = inputstram;
	}

	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		DataInputStream br = new DataInputStream(inputstram);
		
		while(true) {
			
			try {
				
				
				str = br.readUTF();
				//System.out.println(str);
				//Document doc = DocumentHelper.parseText(str);
				System.out.print(str + "\n");
				j = new JSONObject(str);
				Json.ParsingJson(j, p1);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
}
