package snake.game.thread;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.json.JSONObject;

import snake.communication.Json;
import snake.game.Drawing;
import snake.game.Player;
import snake.gui.StartClient;

public class GameStart extends Thread{
	Drawing draw;
	Player p1;
	OutputStream outputstream;
	Boolean running;
	JSONObject json;
	String endmsg = "你輸了!";
	
	
	
	public GameStart(Player p1, Drawing draw, Boolean running) {
		this.p1 = p1;
		this.draw = draw;
		this.running = running;
	}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(running) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				p1.snakeMove();
				if (running == true)
					running = p1.checkEvent();
				else
					endmsg = "你贏了!";
				if (running == true)
					draw.drawGaming(p1.getSnake(), p1.getFood());
				
				
				//建立JSON
				json = Json.BuildsnakeJson(p1.getSnake(), p1.getFood(), p1.getEvent());
				
				//String text = json.toString();
				//System.out.println(text);
				
				Json.SendJson(json, StartClient.getOutputStream());
				
				//XmlGame.SendXml(XmlGame.BuildSnakeXml(p1.getSnake()), outputstream);
				//XmlGame.SendXml(XmlGame.BuildSnakeXml(p2.getSnake()), outputstream);
				System.out.println("GameStart running:"+this.running);
			}
			
			JOptionPane.showMessageDialog(null, endmsg);
		}
	
		
		public void setRunning(Boolean status) {
			this.running = status;
			System.out.println("setRunning:"+this.running);
		}
		
		public Boolean getRunning() {
			return running;
		}
	//接收玩家回傳的事件
		/*
	public class ReceiveEvent implements Runnable{
		
		InputStream inputstram;
		OutputStream outputstream;
		BufferedReader br;
		String str;

		public ReceiveEvent(){
			
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			DataInputStream br = new DataInputStream(inputstram);
			try {
					str = br.readUTF();
					System.out.println(str);
					Document doc = DocumentHelper.parseText(str);
					//XmlGame.ParsingXml(doc, p1);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}*/
	
	
}
